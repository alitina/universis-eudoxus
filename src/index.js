// Universis Project copyright 2020 All rights reserved
const { EudoxusService } = require('./EudoxusService');
const { StudentIdentifierMapper, StudentInstituteIdentifierMapper, UniqueIdentifierMapper } = require('./studentMapper');
const { EudoxusSchemaLoader } = require('./EudoxusSchemaLoader');


module.exports = {
    EudoxusService,
    StudentIdentifierMapper,
    StudentInstituteIdentifierMapper,
    UniqueIdentifierMapper,
    EudoxusSchemaLoader
}
