import {FileSchemaLoaderStrategy} from '@themost/data';
import { ConfigurationBase } from '@themost/common';
export declare class EudoxusSchemaLoader extends FileSchemaLoaderStrategy {
    constructor(config: ConfigurationBase);
}
